﻿using UnityEngine;
using UnityEngine.UI;

namespace RGB_piano
{
    public class KeyView : MonoBehaviour
    {
        [SerializeField]
        private RectTransform rect;
        [SerializeField]
        private Image image;

        private int index;
        private int octaveIndex;
        private bool isBlack;

        private Color initColor;
        private Color pressedColor;

        public float Width
        {
            get { return rect.rect.width; }
        }

        public float LocalPositionX
        {
            get { return rect.localPosition.x; }
        }

        public bool IsBlack
        {
            get { return isBlack; }
        }

        public void Init(float width, int index, int octaveIndex, bool isBlack, Color color)
        {
            this.index = index;
            this.octaveIndex = octaveIndex;
            this.isBlack = isBlack;

            initColor = isBlack ? Color.black : Color.white;
            pressedColor = color;

            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
            if (isBlack)
            {
                rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 0.6f * rect.rect.height);
                Vector3 pos = rect.position;
                pos.z = 10f;
                rect.position = pos;
            }

            ResetColor();

            gameObject.SetActive(true);
        }

        private void ResetColor()
        {
            SetColor(initColor);
        }

        private void SetColor(Color color)
        {
            color.a = 100;
            image.color = color;
        }

        private Color GetPressedColor()
        {
            switch (octaveIndex)
            {
                case 0: return new Color();
                case 1: return new Color();
                case 2: return new Color();
                case 3: return new Color(256,0,0);
                case 4: return new Color(256, 256, 0);
                case 5: return new Color(0, 256, 0);
                case 6: return new Color(0, 256, 256);
                case 7: return new Color(0, 0, 256);
                case 8: return new Color(256, 0, 256);
                case 9: return new Color(256, 0, 0);
                case 10: return new Color();
                case 11: return new Color();
            }
            return Color.black;
        }

        public void KeyDown()
        {
            SetColor(pressedColor);
        }

        public void KeyUp()
        {
            ResetColor();
        }
    }
}
