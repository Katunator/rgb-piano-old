﻿using MidiPlayerTK;
using System.Collections.Generic;
using UnityEngine;

namespace RGB_piano
{
    public class PianoView : MonoBehaviour
    {
        private const int whiteKeyCount = 52;
        private const int octaveNoteCount = 12;

        public static PianoView I;

        [SerializeField]
        private RectTransform rect;
        [SerializeField]
        private RectTransform whiteKeys;
        [SerializeField]
        private RectTransform blackKeys;
        [SerializeField]
        private KeyView keyPrefab;
        [SerializeField]
        private Color[] colors;

        private float keyPositionStepX;
        private float keyWidth;
        private int keyCount
        {
            get { return MusicController.noteCount; }
        }

        // int: note value
        private Dictionary<int, KeyView> keys;

        private void Awake()
        {
            I = this;

            keyWidth = rect.rect.width / whiteKeyCount;
            keys = new Dictionary<int, KeyView>();

            float keyX = 0f;
            bool isBlack;
            int keyValue = MusicController.noteValueFirst;

            for (int i = 0; i < keyCount; i++)
            {
                isBlack = IsBlackKey(i);

                keys.Add(keyValue,
                    InstantiateKey(
                        i,
                        isBlack,
                        isBlack ? keyX - keyWidth / 4f : keyX));

                if (!isBlack)
                {
                    keyX += keyWidth;
                }

                keyValue++;
            }
        }

        private void OnEnable()
        {
            NoteView.OnPlayStart += OnNotePlayStartHandler;
            NoteView.OnPlayFinish += OnNotePlayFinishHandler;
        }

        private void OnDisable()
        {
            NoteView.OnPlayStart -= OnNotePlayStartHandler;
            NoteView.OnPlayFinish -= OnNotePlayFinishHandler;
        }

        private KeyView InstantiateKey(int index, bool isBlack, float positionX)
        {
            KeyView key = Instantiate(keyPrefab, isBlack ? blackKeys : whiteKeys);
            key.transform.localPosition = new Vector3(positionX, 0, 0);

            float width = isBlack ? keyWidth / 2f : keyWidth;

            key.Init(width, index, GetKeyOctaveIndex(index), IsBlackKey(index), GetKeyColor(GetKeyOctaveIndex(index)));

            return key;
        }

        private Vector3 GetKeyPosition(int index)
        {
            return new Vector3(keyPositionStepX * index, 0, 0);
        }

        private bool IsBlackKey(int index)
        {
            switch (GetKeyOctaveIndex(index))
            {
                case 0: return false;
                case 1: return true;
                case 2: return false;
                case 3: return false;
                case 4: return true;
                case 5: return false;
                case 6: return true;
                case 7: return false;
                case 8: return false;
                case 9: return true;
                case 10: return false;
                case 11: return true;
            }

            return false;
        }

        private Color GetKeyColor(int octaveIndex)
        {
            return colors[octaveIndex];
        }

        private int GetKeyOctaveIndex(int index)
        {
            return index % octaveNoteCount;
        }

        public KeyView GetKey(MPTKEvent note)
        {
            return keys[note.Value];
        }

        private void OnNotePlayStartHandler(NoteView note)
        {
            GetKey(note.Note).KeyDown();
        }

        private void OnNotePlayFinishHandler(NoteView note)
        {
            GetKey(note.Note).KeyUp();
        }
    }
}