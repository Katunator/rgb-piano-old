﻿using System.Collections.Generic;
using UnityEngine;
using MidiPlayerTK;
using System;

namespace RGB_piano
{
    public class MusicView : MonoBehaviour
    {
        public static float Speed = 150f;
        public static float noteFallTime;

        [SerializeField]
        private MidiFilePlayer midiFilePlayer;

        [SerializeField]
        private NoteView NoteDisplay;

        [SerializeField]
        private RectTransform rect;
        [SerializeField]
        private RectTransform whiteNotes;
        [SerializeField]
        private RectTransform blackNotes;

        private float noteWidth;
        private float notePositionStepX;

        // int: note value
        private Dictionary<int, NoteView> notes;

        private int noteCount
        {
            get { return MusicController.noteCount; }
        }

        private void Awake()
        {
            notePositionStepX = rect.rect.width / (noteCount + 1);
            noteWidth = rect.rect.width /noteCount;
            noteFallTime = rect.rect.height / Speed;

            InitNoteDictionary();
        }

        private void InitNoteDictionary()
        {
            notes = new Dictionary<int, NoteView>();
            for (int i = MusicController.noteValueFirst; i < MusicController.noteValueLast + 1; i++)
            {
                notes.Add(i, null);
            }
        }

        private void OnEnable()
        {
            NoteView.OnPlayFinish += OnNotePlayFinishHandler;

            SubscribeOnPlayer();
        }

        private void OnDisable()
        {
            NoteView.OnPlayFinish -= OnNotePlayFinishHandler;
        }

        private void SubscribeOnPlayer()
        {
            if (!HelperDemo.CheckSFExists()) return;

            if (midiFilePlayer != null)
            {
                if (!midiFilePlayer.OnEventNotesMidi.HasEvent())
                {
                    midiFilePlayer.OnEventNotesMidi.AddListener(OnEventNoteMidiHandler);
                }
            }
        }

        private void OnEventNoteMidiHandler(List<MPTKEvent> notes)
        {
            foreach (MPTKEvent note in notes)
            {
                if (note.Command == MPTKCommand.NoteOn
                    && note.Value >= MusicController.noteValueFirst
                    && note.Value <= MusicController.noteValueLast)
                {
                    InstantiateNote(note);
                }
            }
        }

        private void InstantiateNote(MPTKEvent note)
        {
            int value = note.Value;
            int index = value - MusicController.noteValueFirst;
            KeyView key = PianoView.I.GetKey(note);

            NoteView n = Instantiate(NoteDisplay, key.IsBlack ? blackNotes : whiteNotes);
            n.transform.localPosition = GetNotePosition(key);

            float height = GetNoteHeight(note.Duration);
            float width = GetNoteWidth(key);

            n.Init(note, width, height, key.IsBlack);

            CheckNoteOverlapping(n, notes[value]);

            notes[value] = n;
        }

        private void CheckNoteOverlapping(NoteView note, NoteView prewNote)
        {
            if (prewNote != null)
            {
                float overlappingDelta = prewNote.LocalPositionHeight - note.LocalPositionY;
                if (overlappingDelta > 0f)
                {
                    prewNote.ChangeHeight(-(overlappingDelta + 0.1f));
                }
            }
        }

        private Vector3 GetNotePosition(KeyView key)
        {
            return new Vector3(
                key.LocalPositionX,
                rect.rect.height,
                0);
        }

        private float GetNoteHeight(float durationMilisec)
        {
            return Speed * durationMilisec * 0.001f;
        }

        private float GetNoteWidth(KeyView key)
        {
            return key.Width;
        }

        private void OnNotePlayFinishHandler(NoteView note)
        {
            if (notes[note.Note.Value] == note)
            {
                notes[note.Note.Value] = null;
            }
        }

        void OnGUI()
        {
            int startx = 5;
            int starty = 120;
            int maxwidth = Screen.width;

            if (!HelperDemo.CheckSFExists()) return;

            if (midiFilePlayer != null)
            {
                GUILayout.BeginArea(new Rect(startx, starty, maxwidth, 200));

                GUILayout.BeginHorizontal();
                if (GUILayout.Button(new GUIContent("Previous", ""), GUILayout.Width(150)))
                {
                    Clear();
                    midiFilePlayer.MPTK_Previous();
                }
                if (GUILayout.Button(new GUIContent("Next", ""), GUILayout.Width(150)))
                {
                    Clear();
                    midiFilePlayer.MPTK_Next();
                }
                if (GUILayout.Button(new GUIContent("Clear", ""), GUILayout.Width(150)))
                    Clear();
                GUILayout.EndHorizontal();
                GUILayout.Label("Midi '" + midiFilePlayer.MPTK_MidiName + (midiFilePlayer.MPTK_IsPlaying ? "' is playing" : " is not playing"));
                GUILayout.BeginHorizontal();
                GUILayout.Label("Midi Position :", GUILayout.Width(100));
                double currentposition = Math.Round(midiFilePlayer.MPTK_Position / 1000d, 2);
                double newposition = Math.Round(GUILayout.HorizontalSlider((float)currentposition, 0f, (float)midiFilePlayer.MPTK_Duration.TotalSeconds, GUILayout.Width(200)), 2);
                if (newposition != currentposition)
                    midiFilePlayer.MPTK_Position = newposition * 1000d;
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("Speed Music :", GUILayout.Width(100));
                float speed = GUILayout.HorizontalSlider(midiFilePlayer.MPTK_Speed, 0.1f, 5f, GUILayout.Width(200));
                if (speed != midiFilePlayer.MPTK_Speed) midiFilePlayer.MPTK_Speed = speed;
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                GUILayout.Label("Speed Note :", GUILayout.Width(100));
                Speed = GUILayout.HorizontalSlider(Speed, 5f, 100f, GUILayout.Width(200));
                GUILayout.EndHorizontal();

                GUILayout.Label("Be careful with the notes traffic jam!!!");

                GUILayout.EndArea();
            }
        }

        public void Clear()
        {
            NoteView[] components = FindObjectsOfType<NoteView>();
            foreach (NoteView noteview in components)
            {
                if (noteview.enabled)
                {
                    DestroyImmediate(noteview.gameObject);
                }
            }
        }
    }
}