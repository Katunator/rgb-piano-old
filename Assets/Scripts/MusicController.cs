﻿using MidiPlayerTK;
using System.Collections;
using UnityEngine;

namespace RGB_piano
{
    public class MusicController : MonoBehaviour
    {
        public const int noteCount = 88;

        public const int noteValueFirst = 21;
        public const int noteValueLast = 108;

        // sound player does more work (not only calculate events but also play them) and works slower as a result
        private const float speedMuliplierForPlayerNotes = 0.9875f;

        [SerializeField] private MidiFilePlayer playerNotes;
        [SerializeField] private MidiFilePlayer playerSound;

        public float speed;

        private void OnValidate()
        {
            playerSound.MPTK_Speed = speed;
            playerNotes.MPTK_Speed = speed * speedMuliplierForPlayerNotes;
        }

        private void Start()
        {
            StartCoroutine(PlaySound());
        }

        private IEnumerator PlaySound()
        {
            playerNotes.MPTK_Play();

            yield return new WaitForSeconds(MusicView.noteFallTime);

            playerSound.MPTK_Play();
        }
    }
}
