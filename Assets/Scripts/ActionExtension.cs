﻿using System;

public static class ActionExtension
{
    public static void SafeInvoke(this Action action)
    {
        if (action != null)
        {
            action.Invoke();
        }
    }

    public static void SafeInvoke<T>(this Action<T> action, T p1)
    {
        if (action != null)
        {
            action.Invoke(p1);
        }
    }
}
