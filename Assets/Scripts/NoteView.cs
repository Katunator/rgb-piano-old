﻿using MidiPlayerTK;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace RGB_piano
{
    public class NoteView : MonoBehaviour
    {
        public static event Action<NoteView> OnPlayStart;
        public static event Action<NoteView> OnPlayFinish;

        [SerializeField]
        private RectTransform rect;
        [SerializeField]
        private Image image;

        private MPTKEvent note;
        private bool isBlack;

        private bool played = false;

        public float LocalPositionY
        {
            get { return rect.localPosition.y; }
        }

        public float LocalPositionHeight
        {
            get { return LocalPositionY + rect.rect.height; }
        }

        public MPTKEvent Note
        {
            get { return note; }
        }

        public void Init(MPTKEvent note, float width, float height, bool isBlack)
        {
            this.note = note;
            this.isBlack = isBlack;

            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
            SetHeight(height);

            SetColor();

            gameObject.SetActive(true);
        }

        public void ChangeHeight(float amount)
        {
            SetHeight(rect.rect.height + amount);
        }

        private void SetHeight(float value)
        {
            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, value);
        }

        private void SetColor()
        {
            image.color = isBlack ? Color.gray : Color.white;
        }

        public void Update()
        {
            float translation = Time.deltaTime * MusicView.Speed;
            transform.Translate(0, -translation, 0);

            if (!played && transform.localPosition.y < 0f)
            {
                OnPlayStart.SafeInvoke(this);

                played = true;
            }
            if (transform.localPosition.y < -rect.rect.height)
            {
                OnPlayFinish.SafeInvoke(this);

                Destroy(gameObject);
            }
        }

        //void FixedUpdate()
        //{
        //    float translation = Time.fixedDeltaTime * MusicView.Speed;
        //    transform.Translate(0, -translation, 0);
        //}
    }
}